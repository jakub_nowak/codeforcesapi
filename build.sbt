import com.typesafe.sbt.packager.archetypes.JavaAppPackaging

lazy val api = project.in(file(".")).enablePlugins(JavaAppPackaging)

maintainer := "Jakub Nowak"

name := "CodeforcesAPI"

scalaVersion := "2.11.8"

sbtVersion := "0.13.11"

val AKKA_VERSION = "2.4.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http-core" % AKKA_VERSION,
  "com.typesafe.akka" %% "akka-http-experimental" % AKKA_VERSION,
  "com.typesafe.akka" %% "akka-http-spray-json-experimental" % AKKA_VERSION
)