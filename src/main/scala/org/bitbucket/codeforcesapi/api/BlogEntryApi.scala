package org.bitbucket.codeforcesapi.api

import akka.actor.ActorSystem
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import org.bitbucket.codeforcesapi.model.dto.BaseResult
import org.bitbucket.codeforcesapi.model.{BlogEntry, Comment, JsonSupport}

import scala.concurrent.{ExecutionContextExecutor, Future}

class BlogEntryApi private[api]()
(implicit val system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor) extends JsonSupport {

  def comments(blogId: Int): Future[List[Comment]] =
    CodeforcesApi.request(s"http://codeforces.com/api/blogEntry.comments?blogEntryId=$blogId").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[Comment]]].map(_.result)
    }

  def view(blogId: Int): Future[BlogEntry] =
    CodeforcesApi.request(s"http://codeforces.com/api/blogEntry.view?blogEntryId=$blogId").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[BlogEntry]].map(_.result)
    }

}
