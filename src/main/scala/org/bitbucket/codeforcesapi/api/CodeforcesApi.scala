package org.bitbucket.codeforcesapi.api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import org.bitbucket.codeforcesapi.model.{JsonSupport, RecentAction}
import org.bitbucket.codeforcesapi.model.dto.BaseResult

import scala.concurrent.{ExecutionContextExecutor, Future}

case class CodeforcesApi
(implicit system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor) extends JsonSupport {

  val blogEntry = new BlogEntryApi()
  val contest = new ContestApi()
  val problemSet = new ProblemSetApi()
  val user = new UserApi()

  def recentActions(maxCount: Int): Future[List[RecentAction]] = {
    require(maxCount <= 100, "Value od maxCount should be at most 100")
    CodeforcesApi.request(s"http://codeforces.com/api/recentActions?maxCount=$maxCount").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[RecentAction]]].map(_.result)
    }
  }

  def shutdown(): Future[Unit] = {
    Http().shutdownAllConnectionPools()
  }

}

object CodeforcesApi {
  private[api] def request(uri: String)
  (implicit system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor): Future[ResponseEntity] = {
    Http().singleRequest(HttpRequest(uri = uri)).flatMap { res =>
      res.status match {
        case OK => Future(res.entity)
        case _ => Unmarshal(res.entity).to[String].map { body =>
          throw new Exception(s"The response status is ${res.status} and response body is $body")
        }
      }
    }
  }
}