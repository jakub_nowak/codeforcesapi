package org.bitbucket.codeforcesapi.api

import akka.actor.ActorSystem
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import org.bitbucket.codeforcesapi.model._
import org.bitbucket.codeforcesapi.model.dto.{BaseResult, StandingDto}

import scala.concurrent.{ExecutionContextExecutor, Future}

class ContestApi private[api]()
(implicit val system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor) extends JsonSupport {

  def hacks(contestId: Int): Future[List[Hack]] =
    CodeforcesApi.request(s"http://codeforces.com/api/contest.hacks?contestId=$contestId").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[Hack]]].map(_.result)
    }

  def list(gym: Boolean = false): Future[List[Contest]] =
    CodeforcesApi.request(s"http://codeforces.com/api/contest.list?gym=$gym").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[Contest]]].map(_.result)
    }

  def ratingChanges(contestId: Int): Future[List[RatingChange]] =
    CodeforcesApi.request(s"http://codeforces.com/api/contest.ratingChanges?contestId=$contestId").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[RatingChange]]].map(_.result)
    }

  def standings(contestId: Int,
                from: Int = 1, count: Int = 1000000000, showUnofficial: Boolean = false): Future[StandingDto] =
    CodeforcesApi
      .request(s"http://codeforces.com/api/contest.standings?contestId=$contestId&from=$from&count=$count&showUnofficial=$showUnofficial")
      .flatMap { entity =>
        Unmarshal(entity).to[BaseResult[StandingDto]].map(_.result)
      }

  def standingsFromRoom(contestId: Int,
                from: Int = 1, count: Int = 1000000000, showUnofficial: Boolean = false, room: Int): Future[StandingDto] =
    CodeforcesApi
      .request(s"http://codeforces.com/api/contest.standings?contestId=$contestId&from=$from&count=$count&showUnofficial=$showUnofficial&room=$room")
      .flatMap { entity =>
        Unmarshal(entity).to[BaseResult[StandingDto]].map(_.result)
      }

  def standingsForHandles(contestId: Int,
                from: Int = 1, count: Int = 1000000000, showUnofficial: Boolean = false, handles: List[String]): Future[StandingDto] = {
    require(handles.size <= 10000)
    CodeforcesApi
      .request(s"http://codeforces.com/api/contest.standings?contestId=$contestId&from=$from&count=$count&showUnofficial=$showUnofficial&handles=${handles.mkString(";")}")
      .flatMap { entity =>
        Unmarshal(entity).to[BaseResult[StandingDto]].map(_.result)
      }
  }

  def standingsForHandlesFromRoom(contestId: Int,
                from: Int = 1, count: Int = 1000000000, showUnofficial: Boolean = false, handles: List[String], room: Int): Future[StandingDto] = {
    require(handles.size <= 10000)
    CodeforcesApi
      .request(s"http://codeforces.com/api/contest.standings?contestId=$contestId&from=$from&count=$count&showUnofficial=$showUnofficial&handles=${handles.mkString(";")}&room=$room")
      .flatMap { entity =>
        Unmarshal(entity).to[BaseResult[StandingDto]].map(_.result)
      }
  }

  def status(contestId: Int, from: Int = 1, count: Int = 1000000000): Future[List[Submission]] =
    CodeforcesApi.request(s"http://codeforces.com/api/contest.status?contestId=$contestId&from=$from&count=$count").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[Submission]]].map(_.result)
    }

  def statusForHandle(contestId: Int, handle: String, from: Int = 1, count: Int = 1000000000): Future[List[Submission]] =
    CodeforcesApi.request(s"http://codeforces.com/api/contest.status?contestId=$contestId&handle=$handle&from=$from&count=$count").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[Submission]]].map(_.result)
    }

}
