package org.bitbucket.codeforcesapi.api

import akka.actor.ActorSystem
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import org.bitbucket.codeforcesapi.model._
import org.bitbucket.codeforcesapi.model.dto.{BaseResult, ProblemDto}

import scala.concurrent.{ExecutionContextExecutor, Future}

class ProblemSetApi private[api]()
(implicit val system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor) extends JsonSupport {

  def problems(tags: List[String]): Future[(List[Problem], List[ProblemStatistics])] =
    CodeforcesApi.request(s"http://codeforces.com/api/problemset.problems?tags=${tags.mkString(";")}").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[ProblemDto]].map(base => (base.result.problems, base.result.problemStatistics))
    }

  def recentStatus(count: Int): Future[List[Submission]] = {
    require(count <= 1000)
    CodeforcesApi.request(s"http://codeforces.com/api/problemset.recentStatus?count=$count").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[Submission]]].map(_.result)
    }
  }

}
