package org.bitbucket.codeforcesapi.api

import akka.actor.ActorSystem
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import org.bitbucket.codeforcesapi.model._
import org.bitbucket.codeforcesapi.model.dto.BaseResult

import scala.concurrent.{ExecutionContextExecutor, Future}

class UserApi private[api]()
(implicit val system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContextExecutor) extends JsonSupport {

  def blogEntries(handle: String): Future[List[BlogEntry]] =
    CodeforcesApi.request(s"http://codeforces.com/api/user.info?handle=$handle").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[BlogEntry]]].map(_.result)
    }

  def friends(onlyOnline: Boolean): Future[List[String]] =
    CodeforcesApi.request(s"http://codeforces.com/api/user.info?onlyOnline=$onlyOnline").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[String]]].map(_.result)
    }

  def info(handles: Seq[String]): Future[List[User]] =
    CodeforcesApi.request(s"http://codeforces.com/api/user.info?handles=${handles.mkString(";")}").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[User]]].map(_.result)
    }

  def ratedList(activeOnly: Boolean = true): Future[List[User]] =
    CodeforcesApi.request(s"http://codeforces.com/api/user.ratedList?activeOnly=$activeOnly").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[User]]].map(_.result)
    }

  def rating(handle: String): Future[List[RatingChange]] =
    CodeforcesApi.request(s"http://codeforces.com/api/user.rating?handle=$handle").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[RatingChange]]].map(_.result)
    }

  def status(handle: String, from: Int = 1, count: Int = 1000000000): Future[List[Submission]] =
    CodeforcesApi.request(s"http://codeforces.com/api/user.status?handle=$handle&from=$from&count=$count").flatMap { entity =>
      Unmarshal(entity).to[BaseResult[List[Submission]]].map(_.result)
    }

}
