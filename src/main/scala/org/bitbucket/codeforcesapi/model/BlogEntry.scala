package org.bitbucket.codeforcesapi.model

case class BlogEntry(
                    id: Int,
                    originalLocale: String,
                    creationTimeSeconds: Int,
                    authorHandle: String,
                    title: String,
                    content: Option[String],
                    locale: String,
                    modificationTimeSeconds: Int,
                    allowViewHistory: Boolean,
                    tags: List[String],
                    rating: Int
                    )