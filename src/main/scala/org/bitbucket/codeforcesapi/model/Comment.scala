package org.bitbucket.codeforcesapi.model

case class Comment(
                  id: Int,
                  creationTimeSeconds: Int,
                  commentatorHandle: String,
                  locale: String,
                  text: String,
                  parentCommentId: Option[Int],
                  rating: Int
                  )
