package org.bitbucket.codeforcesapi.model

case class Contest(
                  id: Int,
                  name: String,
                  `type`: ContestType,
                  phase: Phase,
                  frozen: Boolean,
                  durationSeconds: Int,
                  startTimeSeconds: Option[Int],
                  relativeTimeSeconds: Option[Int],
                  preparedBy: Option[String],
                  websiteUrl: Option[String],
                  description: Option[String],
                  difficulty: Option[Int],
                  kind: Option[String],
                  icpc: Option[String],
                  country: Option[String],
                  city: Option[String],
                  season: Option[String]
                  )

sealed abstract class ContestType

object ContestType {
  def from(str: String): ContestType = str match {
    case "CF" => CF
    case "IOI" => IOI
    case "ICPC" => ICPC
  }

  case object CF extends ContestType
  case object IOI extends ContestType
  case object ICPC extends ContestType
}

sealed abstract class Phase

object Phase {
  def from(str: String): Phase = str match {
    case "BEFORE" => BEFORE
    case "CODING" => CODING
    case "PENDING_SYSTEM_TEST" => PENDING_SYSTEM_TEST
    case "SYSTEM_TEST" => SYSTEM_TEST
    case "FINISHED" => FINISHED
  }

  case object BEFORE extends Phase
  case object CODING extends Phase
  case object PENDING_SYSTEM_TEST extends Phase
  case object SYSTEM_TEST extends Phase
  case object FINISHED extends Phase
}

