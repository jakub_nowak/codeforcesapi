package org.bitbucket.codeforcesapi.model

case class Hack(
               id: Int,
               creationTimeSeconds: Int,
               hacker: Party,
               defender: Party,
               verdict: Option[HackVerdict],
               problem: Problem,
               test: Option[String],
               judgeProtocol: Option[JudgeProtocol]
               )

sealed abstract class HackVerdict

object HackVerdict {
  def from(str: String): HackVerdict = str match {
    case "HACK_SUCCESSFUL" => HACK_SUCCESSFUL
    case "HACK_UNSUCCESSFUL" => HACK_UNSUCCESSFUL
    case "INVALID_INPUT" => INVALID_INPUT
    case "GENERATOR_INCOMPILABLE" => GENERATOR_INCOMPILABLE
    case "GENERATOR_CRASHED" => GENERATOR_CRASHED
    case "IGNORED" => IGNORED
    case "TESTING" => TESTING
    case "OTHER" => OTHER
  }

  case object HACK_SUCCESSFUL extends HackVerdict
  case object HACK_UNSUCCESSFUL extends HackVerdict
  case object INVALID_INPUT extends HackVerdict
  case object GENERATOR_INCOMPILABLE extends HackVerdict
  case object GENERATOR_CRASHED extends HackVerdict
  case object IGNORED extends HackVerdict
  case object TESTING extends HackVerdict
  case object OTHER extends HackVerdict
}


case class JudgeProtocol(manual: String, protocol: String, verdict: String)