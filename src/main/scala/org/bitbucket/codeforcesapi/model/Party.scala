package org.bitbucket.codeforcesapi.model

case class Party(
                contestId: Int,
                members: List[Member],
                participantType: ParticipantType,
                teamId: Option[Int],
                teamName: Option[String],
                ghost: Boolean,
                room: Option[Int],
                startTimeSeconds: Option[Int]
                )

sealed abstract class ParticipantType

object ParticipantType {
  def from(str: String): ParticipantType = str match {
    case "CONTESTANT" => CONTESTANT
    case "PRACTICE" => PRACTICE
    case "VIRTUAL" => VIRTUAL
    case "MANAGER" => MANAGER
    case "OUT_OF_COMPETITION" => OUT_OF_COMPETITION
  }

  case object CONTESTANT extends ParticipantType
  case object PRACTICE extends ParticipantType
  case object VIRTUAL extends ParticipantType
  case object MANAGER extends ParticipantType
  case object OUT_OF_COMPETITION extends ParticipantType
}
