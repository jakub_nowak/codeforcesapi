package org.bitbucket.codeforcesapi.model

case class Problem(
                  contestId: Int,
                  index: String,
                  name: String,
                  `type`: ProblemType,
                  points: Option[Float],
                  tags: List[String]
                  )

sealed abstract class ProblemType

object ProblemType {
  def from(str: String): ProblemType = str match {
    case "PROGRAMMING" => PROGRAMMING
    case "QUESTION" => QUESTION
  }

  case object PROGRAMMING extends ProblemType
  case object QUESTION extends ProblemType
}