package org.bitbucket.codeforcesapi.model

case class ProblemResult(
                        points: Float,
                        penalty: Option[Int],
                        rejectedAttemptCount: Int,
                        `type`: ProblemResultType,
                        bestSubmissionTimeSeconds: Option[Int]
                        )

sealed abstract class ProblemResultType

object ProblemResultType {
  def from(str: String): ProblemResultType = str match {
    case "PRELIMINARY" => PRELIMINARY
    case "FINAL" => FINAL
  }

  case object PRELIMINARY extends ProblemResultType
  case object FINAL extends ProblemResultType
}

