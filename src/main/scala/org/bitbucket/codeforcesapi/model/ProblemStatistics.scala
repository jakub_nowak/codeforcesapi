package org.bitbucket.codeforcesapi.model

case class ProblemStatistics(
                            contestId: Int,
                            index: String,
                            solvedCount: Int
                            )