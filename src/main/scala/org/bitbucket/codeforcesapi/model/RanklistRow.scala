package org.bitbucket.codeforcesapi.model

case class RanklistRow(
                      party: Party,
                      rank: Int,
                      points: Float,
                      penalty: Int,
                      successfulHackCount: Int,
                      unsuccessfulHackCount: Int,
                      problemResults: List[ProblemResult],
                      lastSubmissionTimeSeconds: Option[Int]
                      )