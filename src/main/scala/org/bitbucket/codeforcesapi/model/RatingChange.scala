package org.bitbucket.codeforcesapi.model

case class RatingChange(
                       contestId: Int,
                       contestName: String,
                       handle: String,
                       rank: Int,
                       ratingUpdateTimeSeconds: Int,
                       oldRating: Int,
                       newRating: Int
                       )
