package org.bitbucket.codeforcesapi.model

case class RecentAction(
                       timeSeconds: Int,
                       blogEntry: Option[BlogEntry],
                       comment: Option[Comment]
                       )
