package org.bitbucket.codeforcesapi.model

case class Submission(
                       id: Int,
                       contestId: Int,
                       creationTimeSeconds: Int,
                       relativeTimeSeconds: Int,
                       problem: Problem,
                       author: Party,
                       programmingLanguage: String,
                       verdict: Option[SubmissionVerdict],
                       testset: Testset,
                       passedTestCount: Int,
                       timeConsumedMillis: Int,
                       memoryConsumedBytes: Int
                     )

sealed abstract class SubmissionVerdict

object SubmissionVerdict {
  def from(str: String): SubmissionVerdict = str match {
    case "FAILED" => FAILED
    case "OK" => OK
    case "PARTIAL" => PARTIAL
    case "COMPILATION_ERROR" => COMPILATION_ERROR
    case "RUNTIME_ERROR" => RUNTIME_ERROR
    case "WRONG_ANSWER" => WRONG_ANSWER
    case "PRESENTATION_ERROR" => PRESENTATION_ERROR
    case "TIME_LIMIT_EXCEEDED" => TIME_LIMIT_EXCEEDED
    case "MEMORY_LIMIT_EXCEEDED" => MEMORY_LIMIT_EXCEEDED
    case "IDLENESS_LIMIT_EXCEEDED" => IDLENESS_LIMIT_EXCEEDED
    case "SECURITY_VIOLATED" => SECURITY_VIOLATED
    case "CRASHED" => CRASHED
    case "INPUT_PREPARATION_CRASHED" => INPUT_PREPARATION_CRASHED
    case "CHALLENGED" => CHALLENGED
    case "SKIPPED" => SKIPPED
    case "TESTING" => TESTING
    case "REJECTED" => REJECTED
  }

  case object FAILED extends SubmissionVerdict
  case object OK extends SubmissionVerdict
  case object PARTIAL extends SubmissionVerdict
  case object COMPILATION_ERROR extends SubmissionVerdict
  case object RUNTIME_ERROR extends SubmissionVerdict
  case object WRONG_ANSWER extends SubmissionVerdict
  case object PRESENTATION_ERROR extends SubmissionVerdict
  case object TIME_LIMIT_EXCEEDED extends SubmissionVerdict
  case object MEMORY_LIMIT_EXCEEDED extends SubmissionVerdict
  case object IDLENESS_LIMIT_EXCEEDED extends SubmissionVerdict
  case object SECURITY_VIOLATED extends SubmissionVerdict
  case object CRASHED extends SubmissionVerdict
  case object INPUT_PREPARATION_CRASHED extends SubmissionVerdict
  case object CHALLENGED extends SubmissionVerdict
  case object SKIPPED extends SubmissionVerdict
  case object TESTING extends SubmissionVerdict
  case object REJECTED extends SubmissionVerdict
}


sealed abstract class Testset

object Testset {
  def from(str: String): Testset = str match {
    case "SAMPLES" => SAMPLES
    case "PRETESTS" => PRETESTS
    case "TESTS" => TESTS
    case "CHALLENGES" => CHALLENGES
    case "TESTS1" => TESTS1
    case "TESTS2" => TESTS2
    case "TESTS3" => TESTS3
    case "TESTS4" => TESTS4
    case "TESTS5" => TESTS5
    case "TESTS6" => TESTS6
    case "TESTS7" => TESTS7
    case "TESTS8" => TESTS8
    case "TESTS9" => TESTS9
    case "TESTS10" => TESTS10
  }

  case object SAMPLES extends Testset
  case object PRETESTS extends Testset
  case object TESTS extends Testset
  case object CHALLENGES extends Testset
  case object TESTS1 extends Testset
  case object TESTS2 extends Testset
  case object TESTS3 extends Testset
  case object TESTS4 extends Testset
  case object TESTS5 extends Testset
  case object TESTS6 extends Testset
  case object TESTS7 extends Testset
  case object TESTS8 extends Testset
  case object TESTS9 extends Testset
  case object TESTS10 extends Testset
}

