package org.bitbucket.codeforcesapi.model

final case class User(
               handle: String,
               email: Option[String],
               vkid: Option[String],
               openId: Option[String],
               firstName: Option[String],
               lastName: Option[String],
               country: Option[String],
               city: Option[String],
               organization: Option[String],
               contribution: Int,
               rank: String,
               rating: Int,
               maxRank: String,
               maxRating: Int,
               lastOnlineTimeSeconds: Int,
               registrationTimeSeconds: Int,
               friendOfCount: Int,
               avatar: String,
               titlePhoto: String
               )
