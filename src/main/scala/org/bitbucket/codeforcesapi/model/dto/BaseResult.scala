package org.bitbucket.codeforcesapi.model.dto

case class BaseResult[A](status: String, result: A)
