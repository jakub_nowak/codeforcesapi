package org.bitbucket.codeforcesapi.model.dto

import org.bitbucket.codeforcesapi.model.{ProblemStatistics, Problem}

case class ProblemDto(problems: List[Problem], problemStatistics: List[ProblemStatistics])
