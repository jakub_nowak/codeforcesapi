package org.bitbucket.codeforcesapi.model.dto

import org.bitbucket.codeforcesapi.model.{RanklistRow, Problem, Contest}

case class StandingDto(contest: Contest, problems: List[Problem], rows: List[RanklistRow])