package org.bitbucket.codeforcesapi

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.bitbucket.codeforcesapi.model.dto.{BaseResult, ProblemDto, StandingDto}
import spray.json._

package object model {

  implicit object HackVerdictFormat extends RootJsonFormat[HackVerdict] {
    def write(obj: HackVerdict): JsValue = JsString(obj.toString)

    def read(json: JsValue): HackVerdict = json match {
      case JsString(str) => HackVerdict.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  implicit object ParticipantTypeFormat extends RootJsonFormat[ParticipantType] {
    def write(obj: ParticipantType): JsValue = JsString(obj.toString)

    def read(json: JsValue): ParticipantType = json match {
      case JsString(str) => ParticipantType.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  implicit object ProblemTypeFormat extends RootJsonFormat[ProblemType] {
    def write(obj: ProblemType): JsValue = JsString(obj.toString)

    def read(json: JsValue): ProblemType = json match {
      case JsString(str) => ProblemType.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  implicit object ContestTypeFormat extends RootJsonFormat[ContestType] {
    def write(obj: ContestType): JsValue = JsString(obj.toString)

    def read(json: JsValue): ContestType = json match {
      case JsString(str) => ContestType.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  implicit object PhaseFormat extends RootJsonFormat[Phase] {
    def write(obj: Phase): JsValue = JsString(obj.toString)

    def read(json: JsValue): Phase = json match {
      case JsString(str) => Phase.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  implicit object SubmissionVerdictFormat extends RootJsonFormat[SubmissionVerdict] {
    def write(obj: SubmissionVerdict): JsValue = JsString(obj.toString)

    def read(json: JsValue): SubmissionVerdict = json match {
      case JsString(str) => SubmissionVerdict.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  implicit object TestsetFormat extends RootJsonFormat[Testset] {
    def write(obj: Testset): JsValue = JsString(obj.toString)

    def read(json: JsValue): Testset = json match {
      case JsString(str) => Testset.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  implicit object ProblemResultTypeFormat extends RootJsonFormat[ProblemResultType] {
    def write(obj: ProblemResultType): JsValue = JsString(obj.toString)

    def read(json: JsValue): ProblemResultType = json match {
      case JsString(str) => ProblemResultType.from(str)
      case obj => throw new DeserializationException(s"Object $obj is not a proper enum")
    }
  }

  trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
    implicit val userFormat = jsonFormat19(User)
    implicit val baseUserFormat = jsonFormat2(BaseResult[List[User]])

    implicit val ratingChangeFormat = jsonFormat7(RatingChange)
    implicit val baseRatingChangeFormat = jsonFormat2(BaseResult[List[RatingChange]])

    implicit val commentFormat = jsonFormat7(Comment)
    implicit val baseCommentFormat = jsonFormat2(BaseResult[List[Comment]])

    implicit val memberFormat = jsonFormat1(Member)
    implicit val partyFormat = jsonFormat8(Party)

    implicit val contestFormat = jsonFormat17(Contest)
    implicit val baseContestFormat = jsonFormat2(BaseResult[List[Contest]])

    implicit val problemFormat = jsonFormat6(Problem)
    implicit val problemResultFormat = jsonFormat5(ProblemResult)
    implicit val problemStatisticsFormat = jsonFormat3(ProblemStatistics)
    implicit val problemDtoFormat = jsonFormat2(ProblemDto)
    implicit val baseProblemDtoFormat = jsonFormat2(BaseResult[ProblemDto])

    implicit val judgeFormat = jsonFormat3(JudgeProtocol)
    implicit val hackFormat = jsonFormat8(Hack)
    implicit val baseHackFormat = jsonFormat2(BaseResult[List[Hack]])

    implicit val blogEntryFormat = jsonFormat11(BlogEntry)
    implicit val baseBlogEntryFormat = jsonFormat2(BaseResult[BlogEntry])
    implicit val baseBlogEntryListFormat = jsonFormat2(BaseResult[List[BlogEntry]])

    implicit val submissionEntryFormat = jsonFormat12(Submission)
    implicit val baseSubmissionFormat = jsonFormat2(BaseResult[List[Submission]])

    implicit val ranklistRowFormat = jsonFormat8(RanklistRow)

    implicit val standingDtoFormat = jsonFormat3(StandingDto)
    implicit val baseStandingFormat = jsonFormat2(BaseResult[StandingDto])

    implicit val recentActionFormat = jsonFormat3(RecentAction)
    implicit val baseRecentActionFormat = jsonFormat2(BaseResult[List[RecentAction]])

    implicit val FriendsFormat = jsonFormat2(BaseResult[List[String]])
  }

}